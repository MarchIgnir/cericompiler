#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current;               //current char
char adop;					//save char
char nextchar;				// Next Char

void ReadChar(void){		// Read character and skip spaces until 
				// non space character is read
	while(cin.get(current) && (current==' '||current=='\t'||current=='\n'))
	   	cin.get(current);
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// ArithmeticExpression := Term {AdditiveOperator Term}
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"

void SecondOperator(){
	ReadChar();
	if(adop=='<'){
		if(current=='='||current=='>'){
			nextchar=current;
		}
	}else if(adop=='>'&&current=='='){
		nextchar=current;
	}
}

void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}
		
void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

void ArithmeticExpression(void);
void OperatorRelationnel();			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9'){
			Digit();
	     	ReadChar();

	}else if(current== '<'||current=='>'||current=='='){
	
			OperatorRelationnel();
			ReadChar();
	}


}

void ArithmeticExpression(void){
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}

}

void OperatorRelationnel(){
	Term();
	while(current=='='||current=='<'||current=='>'){
		adop=current;
		if(current=='<'||current=='>'){
			SecondOperator();
		}
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='='){
		 	cout << "\tje %rbx, %rax" << endl; //equal both operands
 		}else if(adop=='<'){
 			if(nextchar=='>'){
 				cout << "\tjne %rbx, %rax" << endl; //different operand
 			}else if(nextchar=='='){
 				cout << "\tjbe %rbx, %rax" << endl; //equal or inferior
 			}else{
 				cout << "\tjb %rbx, %rax" << endl; //inferior
 			}
 		}else if(adop=='>'){
 			if(nextchar=='='){
 				cout << "\tjae %rbx, %rax" << endl; //superior or equal
 			}else{
 				cout << "\tja %rbx, %rax" << endl; //superior
 			}
 		}
	}


}


int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	ArithmeticExpression();
	if(current=='='||current=='<'||current=='>'){
		OperatorRelationnel();
	}
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}